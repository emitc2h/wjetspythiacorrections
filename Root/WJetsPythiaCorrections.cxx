#include "WJetsPythiaCorrections/WJetsPythiaCorrections.h"
#include "TH1F.h"
#include <iostream>

/*
====================================================================================
A class to provide the weights to correct for the mismodelling in delta_eta_tau_lep
and lepton and tau p_T (via the ratio of these two variables) for the following
 W+jets Alpgen+Pythia samples:

mc12_8TeV.147025.AlpgenPythia_Auto_P2011C_WenuNp0.merge.NTUP_TAU.e1879_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147026.AlpgenPythia_Auto_P2011C_WenuNp1.merge.NTUP_TAU.e1879_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147027.AlpgenPythia_Auto_P2011C_WenuNp2.merge.NTUP_TAU.e1879_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147028.AlpgenPythia_Auto_P2011C_WenuNp3.merge.NTUP_TAU.e1879_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147029.AlpgenPythia_Auto_P2011C_WenuNp4.merge.NTUP_TAU.e1879_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147030.AlpgenPythia_Auto_P2011C_WenuNp5incl.merge.NTUP_TAU.e1879_s1581_s1586_r3658_r3549_p1344/

mc12_8TeV.147033.AlpgenPythia_Auto_P2011C_WmunuNp0.merge.NTUP_TAU.e1880_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147034.AlpgenPythia_Auto_P2011C_WmunuNp1.merge.NTUP_TAU.e1880_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147035.AlpgenPythia_Auto_P2011C_WmunuNp2.merge.NTUP_TAU.e1880_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147036.AlpgenPythia_Auto_P2011C_WmunuNp3.merge.NTUP_TAU.e1880_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147037.AlpgenPythia_Auto_P2011C_WmunuNp4.merge.NTUP_TAU.e1880_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147038.AlpgenPythia_Auto_P2011C_WmunuNp5incl.merge.NTUP_TAU.e1880_s1581_s1586_r3658_r3549_p1344/

mc12_8TeV.147041.AlpgenPythia_Auto_P2011C_WtaunuNp0.merge.NTUP_TAU.e1881_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147042.AlpgenPythia_Auto_P2011C_WtaunuNp1.merge.NTUP_TAU.e1881_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147043.AlpgenPythia_Auto_P2011C_WtaunuNp2.merge.NTUP_TAU.e1881_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147044.AlpgenPythia_Auto_P2011C_WtaunuNp3.merge.NTUP_TAU.e1881_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147045.AlpgenPythia_Auto_P2011C_WtaunuNp4.merge.NTUP_TAU.e1881_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147046.AlpgenPythia_Auto_P2011C_WtaunuNp5incl.merge.NTUP_TAU.e1881_s1581_s1586_r3658_r3549_p1344/

This correction has been derived and should be applied to the H->tautau->lephad
ATLAS analysis, in the 0-jet and 1-jet categories.

file   : WJetsPythiaCorrections.cxx
author : Michel Trottier-McDonald <mtm@cern.ch>
date   : May 2013
====================================================================================
*/


//**************************************************************
// Constructor
//**************************************************************
WJetsPythiaCorrections::WJetsPythiaCorrections(const std::string& path) :
    // Initialize members
    root_file_path(path),

    file_pt_ratio(0),
    file_deta(0),

    pt_ratio_OS_0j(0),
    pt_ratio_SS_0j(0),

    pt_ratio_OS_0j_up(0),
    pt_ratio_SS_0j_up(0),

    pt_ratio_OS_0j_down(0),
    pt_ratio_SS_0j_down(0),

    pt_ratio_OS_1j(0),
    pt_ratio_SS_1j(0),

    pt_ratio_OS_1j_up(0),
    pt_ratio_SS_1j_up(0),

    pt_ratio_OS_1j_down(0),
    pt_ratio_SS_1j_down(0),

    deta_OS_0j(0),
    deta_SS_0j(0),

    deta_OS_0j_up(0),
    deta_SS_0j_up(0),

    deta_OS_0j_down(0),
    deta_SS_0j_down(0),

    deta_OS_1j(0),
    deta_SS_1j(0),

    deta_OS_1j_up(0),
    deta_SS_1j_up(0),

    deta_OS_1j_down(0),
    deta_SS_1j_down(0)
{
    load();
}


//**************************************************************
// Load correction functions
//**************************************************************
void WJetsPythiaCorrections::load()
{

    // Load files
    file_pt_ratio = new TFile((root_file_path + "WShapeCorrection_1D_pt_ratio.root").c_str());
    file_deta     = new TFile((root_file_path + "WShapeCorrection_1D_deta.root").c_str());



    // Obtain histograms
    TH1F* h_pt_ratio_OS_0j  = (TH1F*)file_pt_ratio->Get( "W-1D-pt-ratio-OS-0j" );
    TH1F* h_pt_ratio_SS_0j  = (TH1F*)file_pt_ratio->Get( "W-1D-pt-ratio-SS-0j" );

    TH1F* h_pt_ratio_OS_0j_up  = (TH1F*)file_pt_ratio->Get( "W-1D-pt-ratio-OS-0j-up" );
    TH1F* h_pt_ratio_SS_0j_up  = (TH1F*)file_pt_ratio->Get( "W-1D-pt-ratio-SS-0j-up" );

    TH1F* h_pt_ratio_OS_0j_down  = (TH1F*)file_pt_ratio->Get( "W-1D-pt-ratio-OS-0j-down" );
    TH1F* h_pt_ratio_SS_0j_down  = (TH1F*)file_pt_ratio->Get( "W-1D-pt-ratio-SS-0j-down" );


    TH1F* h_pt_ratio_OS_1j  = (TH1F*)file_pt_ratio->Get( "W-1D-pt-ratio-OS-1j" );
    TH1F* h_pt_ratio_SS_1j  = (TH1F*)file_pt_ratio->Get( "W-1D-pt-ratio-SS-1j" );

    TH1F* h_pt_ratio_OS_1j_up  = (TH1F*)file_pt_ratio->Get( "W-1D-pt-ratio-OS-1j-up" );
    TH1F* h_pt_ratio_SS_1j_up  = (TH1F*)file_pt_ratio->Get( "W-1D-pt-ratio-SS-1j-up" );

    TH1F* h_pt_ratio_OS_1j_down  = (TH1F*)file_pt_ratio->Get( "W-1D-pt-ratio-OS-1j-down" );
    TH1F* h_pt_ratio_SS_1j_down  = (TH1F*)file_pt_ratio->Get( "W-1D-pt-ratio-SS-1j-down" );



    TH1F* h_deta_OS_0j  = (TH1F*)file_deta->Get( "W-1D-deta-OS-0j" );
    TH1F* h_deta_SS_0j  = (TH1F*)file_deta->Get( "W-1D-deta-SS-0j" );

    TH1F* h_deta_OS_0j_up  = (TH1F*)file_deta->Get( "W-1D-deta-OS-0j-up" );
    TH1F* h_deta_SS_0j_up  = (TH1F*)file_deta->Get( "W-1D-deta-SS-0j-up" );

    TH1F* h_deta_OS_0j_down  = (TH1F*)file_deta->Get( "W-1D-deta-OS-0j-down" );
    TH1F* h_deta_SS_0j_down  = (TH1F*)file_deta->Get( "W-1D-deta-SS-0j-down" );


    TH1F* h_deta_OS_1j  = (TH1F*)file_deta->Get( "W-1D-deta-OS-1j" );
    TH1F* h_deta_SS_1j  = (TH1F*)file_deta->Get( "W-1D-deta-SS-1j" );

    TH1F* h_deta_OS_1j_up  = (TH1F*)file_deta->Get( "W-1D-deta-OS-1j-up" );
    TH1F* h_deta_SS_1j_up  = (TH1F*)file_deta->Get( "W-1D-deta-SS-1j-up" );

    TH1F* h_deta_OS_1j_down  = (TH1F*)file_deta->Get( "W-1D-deta-OS-1j-down" );
    TH1F* h_deta_SS_1j_down  = (TH1F*)file_deta->Get( "W-1D-deta-SS-1j-down" );




    // Obtain functions
    pt_ratio_OS_0j  = (TF1*)h_pt_ratio_OS_0j->GetFunction("pol3");
    pt_ratio_SS_0j  = (TF1*)h_pt_ratio_SS_0j->GetFunction("pol3");

    pt_ratio_OS_0j_up  = (TF1*)h_pt_ratio_OS_0j_up->GetFunction("pol3");
    pt_ratio_SS_0j_up  = (TF1*)h_pt_ratio_SS_0j_up->GetFunction("pol3");

    pt_ratio_OS_0j_down  = (TF1*)h_pt_ratio_OS_0j_down->GetFunction("pol3");
    pt_ratio_SS_0j_down  = (TF1*)h_pt_ratio_SS_0j_down->GetFunction("pol3");


    pt_ratio_OS_1j  = (TF1*)h_pt_ratio_OS_1j->GetFunction("pol3");
    pt_ratio_SS_1j  = (TF1*)h_pt_ratio_SS_1j->GetFunction("pol3");

    pt_ratio_OS_1j_up  = (TF1*)h_pt_ratio_OS_1j_up->GetFunction("pol3");
    pt_ratio_SS_1j_up  = (TF1*)h_pt_ratio_SS_1j_up->GetFunction("pol3");

    pt_ratio_OS_1j_down  = (TF1*)h_pt_ratio_OS_1j_down->GetFunction("pol3");
    pt_ratio_SS_1j_down  = (TF1*)h_pt_ratio_SS_1j_down->GetFunction("pol3");



    deta_OS_0j  = (TF1*)h_deta_OS_0j->GetFunction("pol3");
    deta_SS_0j  = (TF1*)h_deta_SS_0j->GetFunction("pol3");

    deta_OS_0j_up  = (TF1*)h_deta_OS_0j_up->GetFunction("pol3");
    deta_SS_0j_up  = (TF1*)h_deta_SS_0j_up->GetFunction("pol3");

    deta_OS_0j_down  = (TF1*)h_deta_OS_0j_down->GetFunction("pol3");
    deta_SS_0j_down  = (TF1*)h_deta_SS_0j_down->GetFunction("pol3");


    deta_OS_1j  = (TF1*)h_deta_OS_1j->GetFunction("pol3");
    deta_SS_1j  = (TF1*)h_deta_SS_1j->GetFunction("pol3");

    deta_OS_1j_up  = (TF1*)h_deta_OS_1j_up->GetFunction("pol3");
    deta_SS_1j_up  = (TF1*)h_deta_SS_1j_up->GetFunction("pol3");

    deta_OS_1j_down  = (TF1*)h_deta_OS_1j_down->GetFunction("pol3");
    deta_SS_1j_down  = (TF1*)h_deta_SS_1j_down->GetFunction("pol3");
}

//**************************************************************
// Get weight
//**************************************************************
Float_t WJetsPythiaCorrections::weight(const Float_t& pt_ratio,
                                       const Float_t& delta_eta,
                                       const Int_t& charge_product,
                                       const Bool_t& is_0j_category,
                                       const Int_t& sys_variation_pt_ratio,
                                       const Int_t& sys_variation_deta)
{

    Float_t weight = 1.0;

    // 0j category
    if(is_0j_category)
    {
        // Same sign
        if(charge_product == 1)
        {
            if(sys_variation_pt_ratio == 0 ) weight *= pt_ratio_SS_0j->Eval(pt_ratio);
            if(sys_variation_pt_ratio == 1 ) weight *= pt_ratio_SS_0j_up->Eval(pt_ratio);
            if(sys_variation_pt_ratio == -1) weight *= pt_ratio_SS_0j_down->Eval(pt_ratio);
            
            if(sys_variation_deta == 0 ) weight *= deta_SS_0j->Eval(delta_eta);
            if(sys_variation_deta == 1 ) weight *= deta_SS_0j_up->Eval(delta_eta);
            if(sys_variation_deta == -1) weight *= deta_SS_0j_down->Eval(delta_eta);
        }
        // Opposite sign
        else if(charge_product == -1)
        {
            if(sys_variation_pt_ratio == 0 ) weight *= pt_ratio_OS_0j->Eval(pt_ratio);
            if(sys_variation_pt_ratio == 1 ) weight *= pt_ratio_OS_0j_up->Eval(pt_ratio);
            if(sys_variation_pt_ratio == -1) weight *= pt_ratio_OS_0j_down->Eval(pt_ratio);
            
            if(sys_variation_deta == 0 ) weight *= deta_OS_0j->Eval(delta_eta);
            if(sys_variation_deta == 1 ) weight *= deta_OS_0j_up->Eval(delta_eta);
            if(sys_variation_deta == -1) weight *= deta_OS_0j_down->Eval(delta_eta);
        }
    }

    // 1j and other categories
    else
    {
        // Same sign
        if(charge_product == 1)
        {
            if(sys_variation_pt_ratio == 0 ) weight *= pt_ratio_SS_1j->Eval(pt_ratio);
            if(sys_variation_pt_ratio == 1 ) weight *= pt_ratio_SS_1j_up->Eval(pt_ratio);
            if(sys_variation_pt_ratio == -1) weight *= pt_ratio_SS_1j_down->Eval(pt_ratio);
            
            if(sys_variation_deta == 0 ) weight *= deta_SS_1j->Eval(delta_eta);
            if(sys_variation_deta == 1 ) weight *= deta_SS_1j_up->Eval(delta_eta);
            if(sys_variation_deta == -1) weight *= deta_SS_1j_down->Eval(delta_eta);
        }
        // Opposite sign
        else if(charge_product == -1)
        {
            if(sys_variation_pt_ratio == 0 ) weight *= pt_ratio_OS_1j->Eval(pt_ratio);
            if(sys_variation_pt_ratio == 1 ) weight *= pt_ratio_OS_1j_up->Eval(pt_ratio);
            if(sys_variation_pt_ratio == -1) weight *= pt_ratio_OS_1j_down->Eval(pt_ratio);
            
            if(sys_variation_deta == 0 ) weight *= deta_OS_1j->Eval(delta_eta);
            if(sys_variation_deta == 1 ) weight *= deta_OS_1j_up->Eval(delta_eta);
            if(sys_variation_deta == -1) weight *= deta_OS_1j_down->Eval(delta_eta);
        }
    }

    
    return weight;
}
