#ifndef WJETSPYTHIACORRECTIONS_H
#define WJETSPYTHIACORRECTIONS_H

#include <string>
#include "TF1.h"
#include "TFile.h"

/*
====================================================================================
A class to provide the weights to correct for the mismodelling in delta_eta_tau_lep
and lepton and tau p_T (via the ratio of these two variables) for the following
 W+jets Alpgen+Pythia samples:

mc12_8TeV.147025.AlpgenPythia_Auto_P2011C_WenuNp0.merge.NTUP_TAU.e1879_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147026.AlpgenPythia_Auto_P2011C_WenuNp1.merge.NTUP_TAU.e1879_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147027.AlpgenPythia_Auto_P2011C_WenuNp2.merge.NTUP_TAU.e1879_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147028.AlpgenPythia_Auto_P2011C_WenuNp3.merge.NTUP_TAU.e1879_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147029.AlpgenPythia_Auto_P2011C_WenuNp4.merge.NTUP_TAU.e1879_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147030.AlpgenPythia_Auto_P2011C_WenuNp5incl.merge.NTUP_TAU.e1879_s1581_s1586_r3658_r3549_p1344/

mc12_8TeV.147033.AlpgenPythia_Auto_P2011C_WmunuNp0.merge.NTUP_TAU.e1880_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147034.AlpgenPythia_Auto_P2011C_WmunuNp1.merge.NTUP_TAU.e1880_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147035.AlpgenPythia_Auto_P2011C_WmunuNp2.merge.NTUP_TAU.e1880_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147036.AlpgenPythia_Auto_P2011C_WmunuNp3.merge.NTUP_TAU.e1880_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147037.AlpgenPythia_Auto_P2011C_WmunuNp4.merge.NTUP_TAU.e1880_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147038.AlpgenPythia_Auto_P2011C_WmunuNp5incl.merge.NTUP_TAU.e1880_s1581_s1586_r3658_r3549_p1344/

mc12_8TeV.147041.AlpgenPythia_Auto_P2011C_WtaunuNp0.merge.NTUP_TAU.e1881_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147042.AlpgenPythia_Auto_P2011C_WtaunuNp1.merge.NTUP_TAU.e1881_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147043.AlpgenPythia_Auto_P2011C_WtaunuNp2.merge.NTUP_TAU.e1881_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147044.AlpgenPythia_Auto_P2011C_WtaunuNp3.merge.NTUP_TAU.e1881_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147045.AlpgenPythia_Auto_P2011C_WtaunuNp4.merge.NTUP_TAU.e1881_s1581_s1586_r3658_r3549_p1344/
mc12_8TeV.147046.AlpgenPythia_Auto_P2011C_WtaunuNp5incl.merge.NTUP_TAU.e1881_s1581_s1586_r3658_r3549_p1344/

This correction has been derived and should be applied to the H->tautau->lephad
ATLAS analysis, in the 0-jet and 1-jet categories.

file   : WJetsPythiaCorrections.cxx
author : Michel Trottier-McDonald <mtm@cern.ch>
date   : May 2013
====================================================================================
*/

class WJetsPythiaCorrections
{

public:

    // Constructor/Destructor
    WJetsPythiaCorrections(const std::string& path = "share/");

    // Main method to return the weight
    Float_t weight(const Float_t& pt_ratio,
                   const Float_t& delta_eta,
                   const Int_t& charge_product,
                   const Bool_t& is_0j_category,
                   const Int_t& sys_variation_pt_ratio = 0,
                   const Int_t& sys_variation_deta = 0);

private:

    void load();

    // Path to root files
    std::string root_file_path;

    // Keep pointers to root files
    TFile* file_pt_ratio;
    TFile* file_deta;

    // Internal correction functions for pt_ratio
    TF1* pt_ratio_OS_0j;
    TF1* pt_ratio_SS_0j;

    TF1* pt_ratio_OS_0j_up;
    TF1* pt_ratio_SS_0j_up;

    TF1* pt_ratio_OS_0j_down;
    TF1* pt_ratio_SS_0j_down;

    TF1* pt_ratio_OS_1j;
    TF1* pt_ratio_SS_1j;

    TF1* pt_ratio_OS_1j_up;
    TF1* pt_ratio_SS_1j_up;

    TF1* pt_ratio_OS_1j_down;
    TF1* pt_ratio_SS_1j_down;

    // Internal correction functions for delta_eta
    TF1* deta_OS_0j;
    TF1* deta_SS_0j;

    TF1* deta_OS_0j_up;
    TF1* deta_SS_0j_up;

    TF1* deta_OS_0j_down;
    TF1* deta_SS_0j_down;

    TF1* deta_OS_1j;
    TF1* deta_SS_1j;

    TF1* deta_OS_1j_up;
    TF1* deta_SS_1j_up;

    TF1* deta_OS_1j_down;
    TF1* deta_SS_1j_down;

};

#endif // WJETSPYTHIACORRECTIONS_H
